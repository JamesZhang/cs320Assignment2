#!/bin/bash

# header:
echo Assignment \#2-2, James Zhang, zhangjames389@gmail.com

# get all file names under root directory
F=`find $1 -type f`

# loop through files to find C source files
for C in `echo $F`; do
  O=`file $C|cut -d: -f2`
  if [[ $O = ' C source, ASCII text' ]] ;
  then
    filename=$(basename $C).c
    cp $C $filename
    echo $filename
  fi
done

