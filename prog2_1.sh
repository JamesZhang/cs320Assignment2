#!/bin/bash

# header:
echo Assignment \#2-1, James Zhang, zhangjames389@gmail.com

# get name (and regular expression version of name) from the match in Grades:
N=`grep ',100,100,100' $1|cut -d, -f1-2|sed 's/,/ /'`; echo $N
E=`grep ',100,100,100' $1|cut -d, -f1-2|sed 's/,/\\\\s/'`

# get login & password from Logins:
L=`grep -e $E $2|cut -d, -f2`; echo $L
P=`grep -e $E $2|cut -d, -f3`; echo $P

