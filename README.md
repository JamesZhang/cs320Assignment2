James Zhang
zhangjames389@gmail.com

The second homework assignment for CS320 Fall2016.
This assignment is intended to get the user accustomed to shell scripting.

The user shall, in less than reputable methods, acquire the final project for his class from one of his peers.
This assignment consists of three scripts which will aid the user in his quest of cheating to victory.



**** All files must be made executable before running *****
**** To make exe type into terminal: chmod +x "insert file name" ****



prog2_1.sh:
A script which takes in arguments 'Grades' and 'Logins' and searches for a student that has received '100' on all of their assignments.
It echos out the students name and then determines their account's username and password.

Ran as: ./prog2_1.sh Grades Logins


prog2_2.sh:
A script which scans the stolen user's root directory for all C source programs and copies them onto the directory where the script was ran.
While doing so it echos back to the user(thief) what files were found and copied with a '.c' appended onto it.

Ran as: ./prog2_2.sh cs320assignment3


prog2_3.sh:
After stealing the correct files, this script will determine which file is the one the user(thief) needs.
Taking in four command line arguments, it compiles each of them and finds out which file corresponds to the right assignment.
It then echos back the file and its assignment # back to the user

Ran as: ./prog2_3.sh 0d8649.c 12345.c 13b1eb.c cc1e6c.c
Or for the sake of some slight convenience: ./prog2_3.sh *.c
