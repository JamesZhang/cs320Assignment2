#!/bin/bash

# header:
echo Assignment \#2-3, James Zhang, zhangjames389@gmail.com

for F in $1 $2 $3 $4; do

  # get file basename (i.e. without path)
  B=$(basename $F)

  # get filename without ".C" extension, our executable filename
  E="${B%.*}"

  # compile C source to executable
  gcc -o $E $F -lm

  # get number of decimal places in sample output from executable
  O=`./$E 10`

  if echo $O|grep --quiet -e '\.' - ; then
    O=`./$E 10|cut -d. -f2`
    #echo "output='$O'"
    P=`echo $O|wc -c -|cut -d' ' -f1`
    #echo "places='$P'" #(number of places plus one for the newline)
    # places is one extra because of newline character at end of the output line from echo
    A=$((P-3))
    echo $F Assignment \#$A
  else 
    echo $F Assignment \#4
  fi

done
